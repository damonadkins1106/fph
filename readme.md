# Florida Property Homes



This is a custom web application developed for Luis Cabrera as a personalized real-estate website.



This application is powered by the Laravel framework, currently on version 5.7.x and MySQL Server version 5.7.



# Contribution guidelines



The below guidelines are required in order to contribute code to this repository.



#### Style Guide

With multiple developers working on shared repositories, it is important that everyone follow a consistent style guide so that all code is well structured across all repositories, scripts, functions, etc. 



There are many examples available online for the correct coding styles and practices and we do not need to re-write the book on this. For simplicity, follow the guidelines that are set out in the jQuery library: https://contribute.jquery.org/style-guide/js/ for Javascript related development and PSR-2 for PHP related development: https://www.php-fig.org/psr/psr-2/.



Note: Most IDEs will have the ability to format or beautify the code for you, making most of this automatic.



#### Comments

Leave comments in your code to explain what your code is doing. Comments should be written in such a away that another developer could work on your script and easily take over from you. 



#### Branching

Before making any new code changes, a new `feature` branch needs to be created from the `master` branch. This will create an exact copy of the master branch where customizations can be made, tracked and later merged back into master. The `master` branch should always be a reflection of what is currently deployed on Production and will not be available to all developers to make changes on.



A new feature branch can be created either through Bitbucket.org, through your editor of choice or the command line. Give the branch a descriptive name, starting with the issue number if applicable, followed by a concise title of the issue.



In the event of an entire project needing to move to Production, the feature branch should simply contain the name of the feature. If you are working on a single task that needs to be promoted on it's own, the JIRA issue number with a short description of the issue should be used.

  

#### Commits

Commit early, commit often! Don't try and perfect your code and it's functionality before committing. Git only takes responsibility for your data when you commit. Leave frequent checkpoints that you can always view the history of and return to at a later date and see where things broke or went wrong, if they ever do.



Commit messages should always start with the issue number, where applicable and be followed by a concise but descriptive message of what was changed. Below is an example of an acceptable commit message



```ABC-1234: Fixed an issue preventing picking tickets from printing correctly for Warehouse Transfer Orders.```



This format will allow anyone to quickly find commits related to a specific issue, containing a specific keyword, etc. Commit messages such as `bug fixes` require that the person reads the code changes before knowing what they are about and are not acceptable. 



#### Pull Requests

When you are done with your code changes and want to have these moved to either Production, QA or Dev, submit a pull request from your `feature` branch into the `master`, `qa` or `dev` branch. Provided that the above guidelines have been followed, your pull request will be merged into the environment specific branch, after which the code can be released on the environment.



---



# Installation and Setup



Follow the instructions below to get a local copy of the application running.



### Development Requirements

* PHP 7.1.6 or newer

* [Git](https://git-scm.com/)

* [Composer](https://getcomposer.org/)



### Installation Instructions

1. Clone this repository by running the following command: `insert git clone command`

2. Switch to the `dev` branch: `git checkout origin dev` 

3. Install dependencies with composer by changing to the root directory where the application was installed and run: `composer install`

4. Navigate to your local copy of the site and login with the Administrator Login Details.



### Configuration



In order to get your local environment running the application, a configuration specific to your development machine will be required.



1. Navigate to to project root directory.

2. Copy the `.env.copy` file to a file named `.env`.

3. Update the `APP_URL` and `DB_*` variables to match that of your local environment.



Once the installation is complete and you have gone through the configuration, the local app should load in your browser.



### Documentation

* PHP: http://php.net/

* Laravel: http://laravel.com/docs/5.7/

* Composer: https://getcomposer.org/

* Git: https://git-scm.com/

* MySQL: https://dev.mysql.com/doc/refman/5.7/en/